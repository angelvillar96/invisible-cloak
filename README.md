# Invisibility Cloak
### By Angel Villar Corrales


This project uses image processing techniques in order to simulate an invisibility cloak like the on in the movie "Harry Potter and the Philosopher's Stone"

The program detects an orange blanket and it replaces the pixels with the ones from a previous image, thus appearing invisible. It is important to notice
that the program will only work with this (or a really similar) blanket.
The parameters "lowerBound" and "upperBound" contain the lower and upper HSV values that the program will consider invisble cloak. See the InivisibleCloak.ipynb for more information


## Get this repository

#### Clone

> Clone this repository by running 'https://gitlab.com/angelvillar96/invisible-cloak.git'

#### Requirements

> The code is written in Python 3.7
> The follwing libraries are needed to run the code
> - numpy
> - scipy
> - Jupyter
> - matplotlib
> - OpenCV (cv2)
> - time, os and sys
---

##  Contents

> This repository contains the following files:
> - InivisibleCloak.ipynb: Jupyter notebook that explains how the code works
> - cloakLib.py: python file that runs the algorithm in real life with a video


##

> As shown in the following GIF, the performance still has some flaws
> On the one hand, the performance still has some flaws:
> - The "Invisible Area" looks brighter than the background
> - As the blanket used for the cloak is not homogeneous, some areas and edges are not smoothly detected

> On the other hand: 
> - Complex backgrounds do not significantly make the performance worse
> - The results obtained are still meaningful and satisfactory

![Recordit GIF](cut4GitHub.gif) 



 
